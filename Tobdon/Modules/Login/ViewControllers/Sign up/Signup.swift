//
//  Signup.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/18/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import UIKit
import DropDown

class Signup: UIViewController {

    @IBOutlet weak var tfFullName: UITextField!
    @IBOutlet weak var tfEmailAddress: UITextField!
    @IBOutlet weak var tfMobileNumber: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    
    let countryDropDown = DropDown()
    let countries = ["Canada","Japan","China"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDropDown()
        // Do any additional setup after loading the view.
    }
    @IBAction func onBtnSelectCountry(_ sender: UIButton) {
        self.countryDropDown.show()
    }
    @IBAction func onBtnVerifyMe(_ sender: UIButton) {
        self.validateAndProcessSignUp(sender: sender)
    }
    @IBAction func onBtnSignInNow(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK:- Helper Methods
extension Signup{
    private func validateAndProcessSignUp(sender: UIButton){
        let fullName = self.tfFullName.text ?? ""
        let email = self.tfEmailAddress.text ?? ""
        let mobile = self.tfMobileNumber.text ?? ""
        let country = self.tfCountry.text ?? ""
        let password = self.tfPassword.text ?? ""
        let confirmPassword = self.tfConfirmPassword.text ?? ""
        
        if !Validation.isValidName(fullName){
            Utility.main.showToast(message: Strings.INVALID_NAME.text)
            sender.shake()
            return
        }
        if !Validation.isValidEmail(email){
            Utility.main.showToast(message: Strings.INVALID_EMAIL.text)
            sender.shake()
            return
        }
        if !Validation.isValidPhoneNumber(mobile){
            Utility.main.showToast(message: Strings.INVALID_PHONE.text)
            sender.shake()
            return
        }
        if !Validation.validateStringLength(country){
            Utility.main.showToast(message: Strings.INVALID_COUNTRY.text)
            sender.shake()
            return
        }
        if !Validation.isValidPassword(password){
            Utility.main.showToast(message: Strings.INVALID_PWD.text)
            sender.shake()
            return
        }
        if !Validation.isConfirmPasswordIsEqualToPassword(password: password, confirm: confirmPassword){
            Utility.main.showToast(message: Strings.PWD_DONT_MATCH.text)
            sender.shake()
            return
        }
        self.processSignUp()
    }
    private func setDropDown(){
        self.countryDropDown.dataSource = self.countries
        self.countryDropDown.anchorView = self.tfCountry
        self.countryDropDown.direction = .bottom
        self.countryDropDown.cellHeight = 40
        self.countryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tfCountry.text = self.countries[index]
            self.countryDropDown.deselectRow(index)
        }
    }
}
//MARK:- Services
extension Signup{
    private func processSignUp(){
        
    }
}
