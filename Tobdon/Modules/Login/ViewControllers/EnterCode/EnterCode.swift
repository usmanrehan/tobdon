//
//  EnterCode.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/18/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import UIKit
import PinCodeTextField

class EnterCode: BaseController {

    @IBOutlet weak var viewPinCode: PinCodeTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewPinCode.keyboardType = .numberPad
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        self.validateAndProcessVerifyPinCode(sender: sender)
    }
}
extension EnterCode{
    private func validateAndProcessVerifyPinCode(sender: UIButton){
        let pin = self.viewPinCode.text ?? ""
        if pin.count != 4{
            Utility.main.showToast(message: Strings.INVALID_PIN.text)
            sender.shake()
            return
        }
        self.processVerifyPinCode()
    }
}
extension EnterCode{
    private func processVerifyPinCode(){}
}
