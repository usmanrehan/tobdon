//
//  Login.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/18/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import UIKit

class Login: UIViewController {

    @IBOutlet weak var tfEmailPhone: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnSignIn: UIButton!
    
    @IBAction func onBtnResetNow(_ sender: UIButton) {
        self.pushToForgotPassword()
    }
    @IBAction func onBtnSignIn(_ sender: UIButton) {
        self.validateAndProcessLogin(sender: sender)
    }
    @IBAction func onBtnFacebook(_ sender: UIButton) {
    }
    @IBAction func onBtnInstagram(_ sender: UIButton) {
    }
    @IBAction func onBtnSignUpNow(_ sender: UIButton) {
        self.pushToSignUp()
    }
}
extension Login{
    private func validateAndProcessLogin(sender: UIButton){
        let emailPhone = self.tfEmailPhone.text ?? ""
        let password = self.tfPassword.text ?? ""
        if !Validation.validateStringLength(emailPhone) && !Validation.validateStringLength(password){
            Utility.main.showToast(message: Strings.EMPTY_LOGIN_FIELDS.text)
            sender.shake()
            return
        }
        if emailPhone.contains("@"){//Validate email
            if !Validation.isValidEmail(emailPhone){
                Utility.main.showToast(message: Strings.INVALID_EMAIL.text)
                sender.shake()
                return
            }
            if !Validation.isValidPassword(password){
                Utility.main.showToast(message: Strings.INVALID_PWD.text)
                sender.shake()
                return
            }
            self.processLogin()
        }
        else{
            if !Validation.isValidPhoneNumber(emailPhone){
                Utility.main.showToast(message: Strings.INVALID_PHONE.text)
                sender.shake()
                return
            }
            if !Validation.isValidPassword(password){
                Utility.main.showToast(message: Strings.INVALID_PWD.text)
                sender.shake()
                return
            }
            self.processLogin()
        }
    }
    private func pushToForgotPassword(){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "ForgotPassword")
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToSignUp(){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Signup")
        self.navigationController?.pushViewController(controller, animated: true)
    }

}
//MARK:- Service
extension Login{
    private func processLogin(){
        
    }
}
