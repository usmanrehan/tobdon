//
//  ForgotPassword.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/18/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import UIKit

class ForgotPassword: BaseController {

    @IBOutlet weak var tfEmailAddress: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
   
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        self.validateAndProcessResetPassword(sender:sender)
    }
}
extension ForgotPassword{
    private func validateAndProcessResetPassword(sender:UIButton){
        let email = self.tfEmailAddress.text ?? ""
        if !Validation.isValidEmail(email){
            Utility.main.showToast(message: Strings.INVALID_EMAIL.text)
            sender.shake()
            return
        }
        self.processResetPassword()
    }
    private func pushToEnterCode(){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "EnterCode")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}//EnterCode
extension ForgotPassword{
    private func processResetPassword(){
        self.pushToEnterCode()
    }
}
