//
//  DialogueOptionsTVC.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/20/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import UIKit

class DialogueOptionsTVC: UITableViewCell {

    @IBOutlet weak var lblOption: UILabel!
    
    func setData(option:String){
        self.selectionStyle = .none
        self.lblOption.text = option
    }

}
