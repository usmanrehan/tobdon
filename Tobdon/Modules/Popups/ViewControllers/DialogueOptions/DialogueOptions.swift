//
//  DialogueOptions.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/20/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import UIKit

enum DialogueType{
    case Home
    case ActivityLog
}

class DialogueOptions: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var heightContainerView: NSLayoutConstraint!
    @IBOutlet weak var xPositionContainerView: NSLayoutConstraint!
    @IBOutlet weak var yPositionContainerView: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    let options = ["Follow Back","View Profile","Block User","Report User"]
    var xPosition: CGFloat = 0.0
    var yPosition: CGFloat = 0.0
    var dialogueType = DialogueType.Home
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUI()
    }
    @IBAction func onBtnDismiss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- Helper Methods
extension DialogueOptions{
    private func setUI(){
        let cellsHeight:CGFloat = 40.0 * CGFloat(self.options.count)
        let containerHeight = cellsHeight
        self.heightContainerView.constant = containerHeight
        self.xPositionContainerView.constant = self.xPosition
        switch self.dialogueType{
        case .Home:
            if self.yPosition > self.view.frame.height * 0.675{
                self.yPositionContainerView.constant = self.yPosition - self.heightContainerView.constant - 50.0
            }
            else{
                self.yPositionContainerView.constant = self.yPosition
            }
        case .ActivityLog:
            if self.yPosition > self.view.frame.height * 0.675{
                self.yPositionContainerView.constant = self.yPosition - self.heightContainerView.constant - 80.0
            }
            else{
                self.yPositionContainerView.constant = self.yPosition - 40.0
            }
        }
    }
}
//MARK:- UITableViewDataSource
extension DialogueOptions:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.options.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DialogueOptionsTVC", for: indexPath) as! DialogueOptionsTVC
        cell.setData(option: self.options[indexPath.row])
        return cell
    }
}
