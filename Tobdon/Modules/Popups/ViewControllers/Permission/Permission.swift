//
//  Permission.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/18/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import UIKit

protocol PermissionDelegate {
    func permissionGranted()
}

class Permission: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    
    var titleText = ""
    var descriptionText = ""
    var yesText = ""
    var noText = ""
    var delegate: PermissionDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUI()
    }
    
    @IBAction func onBtnYes(_ sender: UIButton) {
        self.delegate?.permissionGranted()
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onBtnNo(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    private func setUI(){
        self.lblTitle.text = self.titleText
        self.lblDescription.text = self.descriptionText
        self.btnYes.setTitle(self.yesText, for: .normal)
        self.btnNo.setTitle(self.noText, for: .normal)
    }
}
