//
//  SelectUser.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/21/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import UIKit

class SelectUser: UIViewController {
    
    let A_Z = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
}
extension SelectUser:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.A_Z.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectUserTVC", for: indexPath) as! SelectUserTVC
        cell.setData()
        return cell
    }
}
extension SelectUser:UITableViewDelegate{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = GrayHeaderWithTitle.instanceFromNib() as! GrayHeaderWithTitle
        header.setTitle(title: self.A_Z[section])
        return header
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28.0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
}
