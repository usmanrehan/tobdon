//
//  GrayHeaderWithTitle.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/21/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import UIKit

class GrayHeaderWithTitle: UIView {

    @IBOutlet weak var lblTitle: UILabel!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "GrayHeaderWithTitle", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

    func setTitle(title:String){
        self.lblTitle.text = title
    }
}
