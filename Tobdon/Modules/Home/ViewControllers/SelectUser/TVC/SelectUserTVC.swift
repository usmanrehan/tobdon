//
//  SelectUserTVC.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/21/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import UIKit

class SelectUserTVC: UITableViewCell {

    @IBOutlet weak var imgProfile: RoundedImage!
    @IBOutlet weak var lblName: UILabel!
    
    func setData(){
        self.selectionStyle = .none
        self.lblName.text = "Peter Griffin"
    }
    

}
