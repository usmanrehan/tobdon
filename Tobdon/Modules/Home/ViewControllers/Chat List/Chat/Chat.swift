//
//  Chat.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/18/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import UIKit
enum MessageInputType{
    case emoji
    case camera
    case photo
    case video
}

class Chat: UIViewController {
    
    @IBOutlet weak var imgEmoji: UIImageView!
    @IBOutlet weak var imgCamera: UIImageView!
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var lblEmoji: UILabel!
    @IBOutlet weak var lblCamera: UILabel!
    @IBOutlet weak var lblPhoto: UILabel!
    @IBOutlet weak var lblVideo: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tfMessageBox: EmojiTextField!
    
    var isStatusBarHidden = true
    var messageInputType = MessageInputType.emoji
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
        self.registerCells()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnEmoji(_ sender: UIButton) {
        self.tfMessageBox.tag = 1
        self.tfMessageBox.becomeFirstResponder()
        self.messageInputType = .emoji
        self.setUI()
    }
    @IBAction func onBtnCamera(_ sender: UIButton) {
        self.messageInputType = .camera
        self.setUI()
    }
    @IBAction func onBtnPhoto(_ sender: UIButton) {
        self.messageInputType = .photo
        self.setUI()
    }
    @IBAction func onBtnVideo(_ sender: UIButton) {
        self.messageInputType = .video
        self.setUI()
    }
}
//MARK:- Helper Methods
extension Chat{
    private func registerCells(){
        self.tableView.register(UINib(nibName: "Sender", bundle: nil), forCellReuseIdentifier: "Sender")
        self.tableView.register(UINib(nibName: "Receiver", bundle: nil), forCellReuseIdentifier: "Receiver")
    }
    private func setUI(){
        switch self.messageInputType {
        case .emoji:
            self.lblEmoji.textColor = Global.APP_COLOR_RED
            self.lblCamera.textColor = UIColor.darkGray
            self.lblPhoto.textColor = UIColor.darkGray
            self.lblVideo.textColor = UIColor.darkGray
        case .camera:
            self.lblEmoji.textColor = UIColor.darkGray
            self.lblCamera.textColor = Global.APP_COLOR_RED
            self.lblPhoto.textColor = UIColor.darkGray
            self.lblVideo.textColor = UIColor.darkGray
        case .photo:
            self.lblEmoji.textColor = UIColor.darkGray
            self.lblCamera.textColor = UIColor.darkGray
            self.lblPhoto.textColor = Global.APP_COLOR_RED
            self.lblVideo.textColor = UIColor.darkGray
        case .video:
            self.lblEmoji.textColor = UIColor.darkGray
            self.lblCamera.textColor = UIColor.darkGray
            self.lblPhoto.textColor = UIColor.darkGray
            self.lblVideo.textColor = Global.APP_COLOR_RED
        }
    }
}
extension Chat:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row % 2 == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Sender", for: indexPath)
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Receiver", for: indexPath)
            return cell
        }
    }
}
extension Chat:UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.tfMessageBox.tag = 0
    }
}
class EmojiTextField: UITextField {
    override var textInputMode: UITextInputMode? {
        for mode in UITextInputMode.activeInputModes {
            if mode.primaryLanguage == "emoji" {
                if self.tag == 1{
                    return mode
                }
            }
            if mode.primaryLanguage == "text" {
                if self.tag == 0{
                    return mode
                }
            }
        }
        return nil
    }
}
