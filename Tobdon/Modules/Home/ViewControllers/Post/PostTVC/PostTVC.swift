//
//  PostTVC.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/20/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import UIKit

class PostTVC: UITableViewCell {

    @IBOutlet weak var imgProfile: RoundedImage!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imgFeed: UIImageView!
    @IBOutlet weak var heightImageFeed: NSLayoutConstraint!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    func setData(){
        self.selectionStyle = .none
//        if let _ = self.imgFeed{
//            self.imgFeed.removeFromSuperview()
//        }
    }
    
}
