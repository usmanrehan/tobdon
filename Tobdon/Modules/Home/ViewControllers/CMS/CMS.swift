//
//  CMS.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/21/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import UIKit

enum CMSType{
    case about
    case terms
    case privacy
}

class CMS: BaseController {

    @IBOutlet weak var tvDescription: UITextView!
    var cmsType = CMSType.about
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        // Do any additional setup after loading the view.
    }
}
//MARK:- Helper Methods
extension CMS{
    private func setData(){
        switch self.cmsType {
        case .about:
            self.title = "About Us"
        case .terms:
            self.title = "Terms & Conditions"
        case .privacy:
            self.title = "Privacy & Policy"
        }
    }
}
