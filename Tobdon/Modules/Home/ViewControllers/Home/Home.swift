//
//  Home.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/18/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import UIKit

class Home: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Home"
        self.registerCell()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "Home"
    }
}
//MARK:- Helper Methods
extension Home{
    private func registerCell(){
        self.tableView.register(UINib(nibName: "PostTVC", bundle: nil), forCellReuseIdentifier: "PostTVC")
    }
    private func presentDialogueOptions(position:CGPoint){
        let storyboard = AppStoryboard.Popups.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "DialogueOptions") as! DialogueOptions
        controller.xPosition = position.x
        controller.yPosition = position.y
        self.present(controller, animated: true, completion: nil)
    }
}
extension Home:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostTVC", for: indexPath) as! PostTVC
        cell.setData()
        cell.btnMore.tag = indexPath.row
        cell.btnMore.addTarget(self, action: #selector(self.onBtnMore(sender:)), for: .touchUpInside)
        return cell
    }
    @objc func onBtnMore(sender:UIButton){
        guard let window = UIApplication.shared.keyWindow else {return}
        let buttonPosition : CGPoint = sender.convert(sender.bounds.origin, to: window)
        let yPosition = buttonPosition.y - 20.0
        let position = CGPoint(x: 8.0, y: yPosition)
        self.presentDialogueOptions(position: position)
    }
}
