//
//  ActivityLog.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/20/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import UIKit

class ActivityLog: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "Activity Log"
    }
}
extension ActivityLog{
    func presentDialogueOptions(position:CGPoint){
        let storyboard = AppStoryboard.Popups.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "DialogueOptions") as! DialogueOptions
        controller.dialogueType = .ActivityLog
        controller.xPosition = position.x
        controller.yPosition = position.y
        self.present(controller, animated: true, completion: nil)
    }
}
extension ActivityLog:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityLogTVC", for: indexPath) as! ActivityLogTVC
        cell.setData()
        cell.delegate = self
        cell.btnMore.tag = indexPath.row
        cell.btnMore.addTarget(self, action: #selector(self.onBtnMore(sender:)), for: .touchUpInside)
        return cell
    }
    @objc func onBtnMore(sender:UIButton){
        guard let window = UIApplication.shared.keyWindow else {return}
        let buttonPosition : CGPoint = sender.convert(sender.bounds.origin, to: window)
        let yPosition = buttonPosition.y
        let position = CGPoint(x: 8.0, y: yPosition)
        self.presentDialogueOptions(position: position)
    }
}
extension ActivityLog:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
}
extension ActivityLog:BtnMoreDelegate{
    func onBtnMore(cell: ActivityLogTVC) {
        let indexPath = self.tableView.indexPath(for: cell)
        print(indexPath!.row)
        print(indexPath!.section)
    }
}
