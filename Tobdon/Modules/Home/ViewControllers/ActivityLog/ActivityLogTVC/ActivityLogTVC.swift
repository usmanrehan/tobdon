//
//  NewFollowerTVC.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/20/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import UIKit

protocol BtnMoreDelegate: AnyObject {
    func onBtnMore(cell:ActivityLogTVC)
}

class ActivityLogTVC: UITableViewCell {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnMore: UIButton!
    
    weak var delegate: BtnMoreDelegate?
    
    func setData(){
        self.selectionStyle = .none
        //self.imgProfile
        self.lblName.text = "John"
        self.lblDescription.text = "started following you"
        self.lblTime.text = "10 min ago"
    }
    @IBAction func onBtnMore(_ sender: UIButton) {
        self.delegate?.onBtnMore(cell: self)
    }
}
