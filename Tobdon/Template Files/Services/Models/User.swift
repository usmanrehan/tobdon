//
//  User.swift
//
//  Created by Hamza Hasan on 12/11/18
//  Copyright (c) . All rights reserved.
//
import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class User: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let dob = "dob"
    static let gymID = "gymID"
    static let language = "language"
    static let lastName = "lastName"
    static let userId = "userId"
    static let isVerified = "isVerified"
    static let fullName = "fullName"
    static let countryCode = "countryCode"
    static let roleID = "roleID"//3trainer //4user
    static let city = "city"
    static let email = "email"
    static let phoneNumber = "phoneNumber"
    static let address = "address"
    static let gender = "gender"
    static let authToken = "authToken"
    static let height = "height"
    static let gymAssigned = "gymAssigned"
    static let notification = "notification"
    static let about = "about"
    static let weight = "weight"
    static let firstName = "firstName"
    static let profileImagePath = "profileImagePath"
    static let zipCode = "zipCode"
    static let age = "age"
    static let notificationCount = "notificationCount"
  }

  // MARK: Properties
  @objc dynamic var dob: String? = ""
  @objc dynamic var gymID = 0
  @objc dynamic var language: String? = ""
  @objc dynamic var lastName: String? = ""
  @objc dynamic var userId = 0
  @objc dynamic var isVerified = false
  @objc dynamic var fullName: String? = ""
  @objc dynamic var countryCode = 0
  @objc dynamic var roleID = 0
  @objc dynamic var city: String? = ""
  @objc dynamic var email: String? = ""
  @objc dynamic var phoneNumber: String? = ""
  @objc dynamic var address: String? = ""
  @objc dynamic var gender = 0
  @objc dynamic var authToken: String? = ""
  @objc dynamic var height: String? = ""
  @objc dynamic var gymAssigned = false
  @objc dynamic var notification: String? = ""
  @objc dynamic var about: String? = ""
  @objc dynamic var weight: String? = ""
  @objc dynamic var firstName: String? = ""
  @objc dynamic var profileImagePath: String? = ""
  @objc dynamic var zipCode: String? = ""
  @objc dynamic var age: String? = ""
  @objc dynamic var notificationCount = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "roleID"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    dob <- map[SerializationKeys.dob]
    gymID <- map[SerializationKeys.gymID]
    language <- map[SerializationKeys.language]
    lastName <- map[SerializationKeys.lastName]
    userId <- map[SerializationKeys.userId]
    isVerified <- map[SerializationKeys.isVerified]
    fullName <- map[SerializationKeys.fullName]
    countryCode <- map[SerializationKeys.countryCode]
    roleID <- map[SerializationKeys.roleID]
    city <- map[SerializationKeys.city]
    email <- map[SerializationKeys.email]
    phoneNumber <- map[SerializationKeys.phoneNumber]
    address <- map[SerializationKeys.address]
    gender <- map[SerializationKeys.gender]
    authToken <- map[SerializationKeys.authToken]
    height <- map[SerializationKeys.height]
    gymAssigned <- map[SerializationKeys.gymAssigned]
    notification <- map[SerializationKeys.notification]
    about <- map[SerializationKeys.about]
    weight <- map[SerializationKeys.weight]
    firstName <- map[SerializationKeys.firstName]
    profileImagePath <- map[SerializationKeys.profileImagePath]
    zipCode <- map[SerializationKeys.zipCode]
    age <- map[SerializationKeys.age]
    notificationCount <- map[SerializationKeys.notificationCount]
  }


}
