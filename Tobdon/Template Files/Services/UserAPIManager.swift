import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class UsersAPIManager: APIManagerBase {
    
    /*
    //MARK:- /Login
    func LoginUser(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.ValidateUser.rawValue, params: params)! as URL
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- /Sign up
    func SignUpUser(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.Signup.rawValue)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- /ForgotPassword
    func ForgotPassword(params: Parameters, success: @escaping DefaultStringResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.ForgotPassword.rawValue, params: params)! as URL
        self.getRequestForStringWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- /VerifyOTPcodeByEmail
    func VerifyOTPcodeByEmail(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.VerifyOTPcodeByEmail.rawValue, params: params)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- /ResendCode
    func ResendCode(params: Parameters, success: @escaping DefaultStringResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.ResendCode.rawValue, params: params)! as URL
        self.getRequestForStringWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- ChangePassword
    func ChangePassword(params: Parameters, success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.ChangePassword.rawValue, params: params)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- Genral Get API Request
    func getRequestForGernal_APIs(params: Parameters, isHeader: Bool, webURL: String, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = GETURLfor(route: webURL)!
        self.getRequestWith(route: route, success: success, failure: failure, withHeader: isHeader)
    }
    
    func getRequestMethode(webURL: String, isHeader: Bool, success:@escaping DefaultAPISuccessClosure, failure:@escaping DefaultAPIFailureClosure){
        let route: URL = GETURLfor(route: webURL)!
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: isHeader)
    }
    //MARK:- User/EditUserProfile
    func EditUserProfile(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.UpdateProfile.rawValue)! as URL
        self.postRequestWithMultipart(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /Login
    func ToggleNotification(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.ToggleNotification.rawValue, params: params)! as URL
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- Get CMS
    func getCMS(params: Parameters,success:@escaping DefaultAPISuccessClosure, failure:@escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.GetCMS.rawValue, params: params)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
//    //MARK:- ChangePassword
//    func UserChangePassword(params: Parameters, success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure){
//        let route: URL = URLforRoute(route: Route.UserChangePassword.rawValue, params: params)! as URL
//        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
//    }
    //MARK:- Get UserNotification
    func UserNotification(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.UserNotifications.rawValue, params: [:])! as URL
        self.getRequestWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- GetUserExerciseLog
    func GetUserExerciseLog(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.GetUserExerciseLog.rawValue, params: params)! as URL
        self.getRequestWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- GetGymDetails
    func getGymDetails(params: Parameters,success:@escaping DefaultAPISuccessClosure, failure:@escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.GetGymDetails.rawValue, params: params)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- GetMachineByQrCode
    func getMachineByQrCode(params: Parameters,success:@escaping DefaultAPISuccessClosure, failure:@escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.GetMachineByQrCode.rawValue, params: params)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- Logout
    func Logout(params: Parameters,success:@escaping DefaultAPISuccessClosure, failure:@escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.Logout.rawValue, params: params)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /CallForHelp
    func CallForHelp(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.CallForHelp.rawValue, params: params)! as URL
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- GetUserExerciseLog
    func GetAllRequestSupport(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.GetAllRequestSupport.rawValue, params: params)! as URL
        self.getRequestWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- GetAllSupportLog
    func GetAllSupportLog(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.GetAllSupportLog.rawValue, params: params)! as URL
        self.getRequestWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- AcceptRequestSupport
    func AcceptRequestSupport(params: Parameters,success:@escaping DefaultAPISuccessClosure, failure:@escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.AcceptRequestSupport.rawValue, params: params)! as URL
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- MarkJobDone
    func MarkJobDone(params: Parameters,success:@escaping DefaultAPISuccessClosure, failure:@escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.MarkJobDone.rawValue, params: params)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- InsertUserExercise
    func InsertUserExercise(params: Parameters,success:@escaping DefaultAPISuccessClosure, failure:@escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.InsertUserExercise.rawValue, params: params)! as URL
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK:- GetUserExerciseBodyParts
    func GetUserExerciseBodyParts(params: Parameters,success:@escaping DefaultStringResultAPISuccesClosure, failure:@escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.GetUserExerciseBodyParts.rawValue, params: params)! as URL
        self.getRequestForStringWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- UserNotificationCount
    func UserNotificationCount(params: Parameters, success: @escaping DefaultIntResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.UserNotificationCount.rawValue, params: params)! as URL
        self.postRequestForIntWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- UpdateUserNotificationCount
    func UpdateUserNotificationCount(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.UpdateUserNotificationCount.rawValue, params: params)! as URL
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
*/

}

