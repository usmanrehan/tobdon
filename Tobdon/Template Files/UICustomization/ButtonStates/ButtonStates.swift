//
//  ButtonStates.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/18/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import Foundation
import UIKit

class RedWhiteButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.borderWidth = 1
        self.borderColor = Global.APP_COLOR_RED
        self.cornerRadius = 8
        self.clipsToBounds = true
        if self.isSelected{
            self.setTitleColor(.white, for: .selected)
            self.backgroundColor = Global.APP_COLOR_RED
        }
        else{
            self.setTitleColor(Global.APP_COLOR_RED, for: .normal)
            self.backgroundColor = .white
        }
    }
}
